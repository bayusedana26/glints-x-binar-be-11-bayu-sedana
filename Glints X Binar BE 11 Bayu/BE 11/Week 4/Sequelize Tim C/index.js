const express = require("express"); // Import express
const app = express();

// Import routes
// const barangRoute = require("./routes/barangRoute");
// const pelangganRoute = require("./routes/pelangganRoute");
// const pemasokRoute = require("./routes/pemasokRoute");
const transaksiRoute = require("./routes/transaksiRoute");

// Enable body parser
app.use(express.json());
app.use(express.urlencoded({ extended: true }));

// Make a public directory for images
app.use(express.static("Public"));

// Import utils
require("./utils/associations");

// Using routes
// app.use("/barang", barangRoute);
// app.use("/pelanggan", pelangganRoute);
// app.use("/pemasok", pemasokRoute);
app.use("/transaksi", transaksiRoute);

// // Test awal2
// app.get("/", (req, res) => {
//   res.send("Hello, world!");
// });

// Server running
app.listen(3000, () => console.log("Connected to localhost:3000"));
