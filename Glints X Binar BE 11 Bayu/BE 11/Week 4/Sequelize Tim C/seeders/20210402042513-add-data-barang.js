"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.bulkInsert("barang", [
      {
        nama: "Sausage",
        harga: 18000,
        id_pemasok: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Chicken",
        harga: 23000,
        id_pemasok: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Beef",
        harga: 50000,
        id_pemasok: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Nugget",
        harga: 30000,
        id_pemasok: 4,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.bulkDelete("barang", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
