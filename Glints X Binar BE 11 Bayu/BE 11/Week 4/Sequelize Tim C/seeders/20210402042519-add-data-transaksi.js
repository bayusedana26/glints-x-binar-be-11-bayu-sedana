"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    queryInterface.bulkInsert("transaksi", [
      {
        id_barang: 1,
        id_pelanggan: 1,
        jumlah: 3,
        total: 54000,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_barang: 2,
        id_pelanggan: 2,
        jumlah: 5,
        total: 115000,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_barang: 3,
        id_pelanggan: 3,
        jumlah: 4,
        total: 200000,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        id_barang: 4,
        id_pelanggan: 4,
        jumlah: 6,
        total: 180000,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    queryInterface.bulkDelete("transaksi", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
