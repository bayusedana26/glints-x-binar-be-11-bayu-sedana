"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert(
      "pemasok",[
        {
          nama: "Fikri",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nama: "Ebit",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nama: "Najmul",
          createdAt: new Date(),
          updatedAt: new Date(),
        },
        {
          nama: "Lia",
          createdAt: new Date(),
          updatedAt: new Date(),
        }
      ]
    );
    /**
     * Add seed commands here.
     *
     * Example:
     * await queryInterface.bulkInsert('People', [{
     *   name: 'John Doe',
     *   isBetaMember: false
     * }], {});
     */
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("pemasok", null, {});
    /**
     * Add commands to revert seed here.
     *
     * Example:
     * await queryInterface.bulkDelete('People', null, {});
     */
  },
};
