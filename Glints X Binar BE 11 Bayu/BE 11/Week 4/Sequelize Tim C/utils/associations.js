const { barang, pelanggan, pemasok, transaksi } = require("../models"); // Import models

// Barang X Pemasok
pemasok.hasMany(barang, { foreignKey: "id_pemasok" });
barang.belongsTo(pemasok, { foreignKey: "id_pemasok" });

// Transaksi X Barang
barang.hasMany(transaksi, { foreignKey: "id_barang" });
transaksi.belongsTo(barang, { foreignKey: "id_barang" });

// Transaksi X Pelanggan
pelanggan.hasMany(transaksi, { foreignKey: "id_pelanggan" });
transaksi.belongsTo(pelanggan, { foreignKey: "id_pelanggan" });
