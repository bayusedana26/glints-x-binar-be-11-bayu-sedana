const { barang, pelanggan, pemasok, transaksi } = require("../models"); // Import models

class TransaksiController {
  // Get all
  async getAll(req, res) {
    try {
      let transaksiData = await transaksi.find()({
        // Choose showed data
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]],
        // Include Barang, Pemasok and Pelanggan
        include: [
          {
            model: barang,
            attribute: ["nama", "harga"],
            include: [
              {
                model: pemasok,
                attribute: ["nama"],
              },
              {
                model: pelanggan,
                attribute: ["nama"],
              },
            ],
          },
        ],
      });
      // If transaksi Data false
      if (transaksiData.length === 0) {
        return res.status(404).json({
          message: "Transaksi doesn't exist",
        });
      } // If found transaksi
      return res.status(200).json({
        message: "Transaksi found",
        transaksiData,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  } // Get One
  async getOne(req, res) {
    transaksi
      .findOne({
        where: { id: req.params.id }, // Enable id
        // Choose showed data
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]],
        // Include Barang, Pelanggan and Pemasok
        include: [
          {
            model: barang,
            attributes: ["nama", "harga"],
            include: [
              {
                model: pemasok,
                attributes: ["nama"],
                include: [
                  {
                    model: pelanggan,
                    attributes: ["nama"],
                  },
                ],
              },
            ],
          },
        ],
      })
      .then((_transaksiData) => {
        // If transaksi doesn't found'
        if (!transaksiData) {
          return res.status(404).json({
            message: "Transaksi doesn't exist",
          });
        }
        return res.status(200).json({
          message: "Transaksi Found",
          transaksiData,
        });
      })
      .catch((e) => {
        return res.status(500).json({
          message: "Internal Server Error",
          error: e,
        });
      });
  }
}

module.exports = new TransaksiController();
