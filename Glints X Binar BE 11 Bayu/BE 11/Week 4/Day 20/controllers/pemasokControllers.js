const { transaksi, barang, pelanggan, pemasok } = require("../models"); // Import all models
require("../utils/associations.js"); // Import table relationship

class PemasokController {
  async getAll(req, res) {
    try {
      const data = await pemasok.findAll({
        // find all data of Pemasok table
        attributes: ["id", "nama", ["createdAt", "waktu"]], // just these attributes that showed
      });

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getOne(req, res) {
    try {
      const data = await pemasok.findOne({
        where: { id: req.params.id },
        attributes: ["id", "nama", ["createdAt", "waktu"]],
      });
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async createPemasok(req, res) {
    try {
      const insertData = await pemasok.create({
        nama: req.body.nama_pemasok,
      });
      return res.status(200).json({
        message: "Success",
        data: insertData,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async updatePemasok(req, res) {
    try {
      await pemasok.update(
        {
          nama: req.body.nama_pemasok,
        },
        { where: { id: req.params.id } }
      );
      // you need this to show the updated item
      const updated = await pemasok.findOne({
        where: { id: req.params.id },
        attributes: ["id", "nama", ["createdAt", "waktu"]],
      });
      return res.status(200).json({
        message: "Success",
        data: updated,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async deletePemasok(req, res) {
    try {
      await pemasok.destroy({
        where: { id: req.params.id },
      });
      const data = await pemasok.findAll({
        // find all data of Transaksi table
        attributes: ["id", "nama", ["createdAt", "waktu"]], // just these attributes that showed
      });

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new PemasokController();
