const { transaksi, barang, pelanggan, pemasok } = require("../models"); // Import all models
require("../utils/associations"); // Import table relationship

class TransaksiController {
  // Get all transaksi data

  async getAll(req, res) {
    try {
      let data = await transaksi.findAll({
        // find all data of Transaksi table
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]], // just these attributes that showed
        include: [
          // Include is join
          {
            model: barang,
            attributes: ["nama"], // just this attrubute from Barang that showed
            include: [
              // Include is join
              { model: pemasok, attributes: ["nama"] },
            ],
          },
          {
            model: pelanggan,
            attributes: ["nama"], // just this attrubute from Pelanggan that showed
          },
        ],
      });

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async getOne(req, res) {
    transaksi
      .findOne({
        // find one data of Transaksi table
        where: {
          id: req.params.id, // where id of Transaksi table is equal to req.params.id
        },
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]], // just these attributes that showed
        include: [
          {
            model: barang,
            attributes: ["nama"], // just this attrubute from Barang that showed
          },
          {
            model: pelanggan,
            attributes: ["nama"], // just this attrubute from Pelanggan that showed
          },
        ],
      })
      .then((data) => {
        if (!data) {
          return res.status(404).json({
            message: "Not Found",
          });
        }
        return res.status(200).json({
          message: "Success",
          data,
        });
      })
      .catch((e) => {
        return res.status(500).json({
          message: "Internal Server Error",
          error: e,
        });
      });
  }
  async create(req, res) {
    try {
      // Find barang and pelanggan
      let findData = await Promise.all([
        barang.findOne({ where: { id: req.body.id_barang } }),
        pelanggan.findOne({ where: { id: req.body.id_pelanggan } }),
      ]);

      let errors = [];

      // If barang not found
      if (!findData[0]) {
        errors.push("Barang Not Found");
      }

      // If pelanggan not found
      if (!findData[1]) {
        errors.push("Pelanggan Not Found");
      }

      // If errors.length > 0
      if (errors.length > 0) {
        return res.status(404).json({
          message: errors.join(", "),
        });
      }

      let price = findData[0].harga;
      let total = eval(price * req.body.jumlah);

      let createdData = await transaksi.create({
        id_barang: req.body.id_barang,
        id_pelanggan: req.body.id_pelanggan,
        jumlah: req.body.jumlah,
        total,
      });

      let data = await transaksi.findOne({
        where: { id: createdData.id },
        attributes: ["id", "jumlah", "total", ["createdAt", "waktu"]], // just these attributes that showed
        include: [
          // Include is join
          {
            model: barang,
            attributes: ["nama", "harga"], // just this attrubute from Barang that showed
            include: [
              // Include is join
              { model: pemasok, attributes: ["nama"] },
            ],
          },
          {
            model: pelanggan,
            attributes: ["nama"], // just this attrubute from Pelanggan that showed
          },
        ],
      });

      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async delete(req, res) {
    try {
      // Delete data
      let data = await transaksi.destroy({ where: { id: req.params.id } });

      // If data deleted is null
      if (!data) {
        return res.status(404).json({
          message: "Transaksi Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success delete transaksi",
      });
    } catch (e) {
      // If error
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new TransaksiController();
