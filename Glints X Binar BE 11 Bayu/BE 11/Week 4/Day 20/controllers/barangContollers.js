const { transaksi, barang, pemasok, pelanggan } = require("../models"); // Import models
require("../utils/associations"); // import relationship

class barangController {
  async getAll(req, res) {
    try {
      const data = await barang.findAll({
        attribute: ["id", "nama", "harga", ["createdAt", "waktu"]],
        include: [
          {
            model: pemasok,
            attribute: ["nama"],
          },
        ],
      });
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal server error",
        error: e,
      });
    }
  }
  async getOne(req, res) {
    try {
      const data = await barang.findOne({
        where: { id: req.params.id },
        attribute: ["id", "nama", "harga", ["createdAt", "waktu"]],
        include: [
          {
            model: pemasok,
            attribute: ["nama"],
          },
        ],
      });
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal server error",
        error: e,
      });
    }
  }
  async createBarang(req, res) {
    try {
      const pemasokBaru = await pemasok.findOne({
        where: { nama: req.body.nama_pemasok },
      });
      const createData = await barang.create({
        nama: req.body.nama_barang,
        harga: req.body.harga_barang,
        id_pemasok: pemasokBaru.id,
      });
      return res.status(200).json({
        message: "Success",
        data: createData,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async deleteBarang(req, res) {
    try {
      await barang.destroy({
        where: { id: req.params.id },
      });
      const data = await barang.findAll({
        attributes: ["id", "nama", "harga", ["createdAt", "waktu"]],
        include: [
          // Include is join
          {
            model: pemasok,
            attributes: ["nama"],
          },
        ],
      });

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new barangController();
