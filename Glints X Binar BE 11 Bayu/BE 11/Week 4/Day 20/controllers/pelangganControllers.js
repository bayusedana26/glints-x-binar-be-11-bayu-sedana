const { transaksi, barang, pelanggan, pemasok } = require("../models"); // Import all models
require("../utils/associations.js"); // Import table relationship

class PelangganController {
  async getAll(req, res) {
    try {
      const data = await pelanggan.findAll({
        // find all data of Pemasok table
        attributes: ["id", "nama", ["createdAt", "waktu"]], // just these attributes that showed
      });

      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async getOne(req, res){
    try{
      const data = await pelanggan.findOne({
        where: {id: req.params.id},
        attributes: ["id", "nama", ["createdAt", "waktu"]],
      })
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch(e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async createPelanggan(req, res){
    try {
      const insertData = await pelanggan.create({
        nama: req.body.nama_pelanggan,
      })
      return res.status(200).json({
        message: "Success",
        data: insertData,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async updatePelanggan(req, res){
    try {
      await pelanggan.update({
        nama: req.body.nama_pelanggan,
      },{where: {id: req.params.id}})
      // you need this to show the updated item
      const updated = await pelanggan.findOne({
        where: {id: req.params.id},
        attributes: ["id", "nama", ["createdAt", "waktu"]],
      })
      return res.status(200).json({
        message: "Success",
        data: updated,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  async deletePelanggan(req, res){
    try {
      await pelanggan.destroy({
        where: {id: req.params.id}
      })
      const data = await pelanggan.findAll({
        // find all data of Transaksi table
        attributes: ["id", "nama", ["createdAt", "waktu"]], // just these attributes that showed
      });

      return res.status(200).json({
        message: "Success",
        data
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new PelangganController();
