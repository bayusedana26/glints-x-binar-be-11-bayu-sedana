"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Barang", [
      {
        nama: "Beef Wellington",
        harga: 15000,
        id_pemasok: 1,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Wagyu",
        harga: 24600,
        id_pemasok: 2,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Sirloin",
        harga: 44500,
        id_pemasok: 3,
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Barang", null, {});
  },
};
