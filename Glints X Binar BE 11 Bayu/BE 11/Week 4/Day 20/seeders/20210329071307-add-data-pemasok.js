"use strict";

module.exports = {
  up: async (queryInterface, Sequelize) => {
    return queryInterface.bulkInsert("Pemasok", [
      {
        nama: "Banu",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Bima",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
      {
        nama: "Mada",
        createdAt: new Date(),
        updatedAt: new Date(),
      },
    ]);
  },

  down: async (queryInterface, Sequelize) => {
    return queryInterface.bulkDelete("Pemasok", null, {});
  },
};
