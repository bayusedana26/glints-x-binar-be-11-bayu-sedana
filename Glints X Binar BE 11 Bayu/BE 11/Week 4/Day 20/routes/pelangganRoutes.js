const express = require("express");
const router = express.Router()
const pelangganController = require("../controllers/pelangganControllers");


router.get("/", pelangganController.getAll);
router.get("/:id", pelangganController.getOne);
// router.post("/", pelangganController.create);
// router.put("/", pelangganontroller.update);
// router.delete("/", pelangganController.delete);

module.exports = router;