const express = require("express");
const router = express.Router()
const pemasokController = require("../controllers/pemasokControllers");


router.get("/", pemasokController.getAll);
router.get("/:id", pemasokController.getOne);
// router.post("/", pemasokController.create);
// router.put("/", pemasokController.update);
// router.delete("/", pemasokController.delete);

module.exports = router;