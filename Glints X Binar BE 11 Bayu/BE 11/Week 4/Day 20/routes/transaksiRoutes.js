const express = require("express"); // Import express
const router = express.Router(); // Make router from app
const TransaksiController = require("../controllers/transaksiControllers.js"); // Import TransaksiController

router.get("/", TransaksiController.getAll); // If accessing localhost:3000/transaksi, it will call getAll function in TransaksiController class
router.get("/:id", TransaksiController.getOne); // If accessing localhost:3000/transaksi/:id, it will call getOne function in TransaksiController class
router.post("/", TransaksiController.create); // If accessing localhost:3000/transaksi
// router.put("/", TransaksiController.update); // If accessing localhost:3000/transaksi
router.delete("/:id", TransaksiController.delete); // If accessing localhost
module.exports = router; // Export router
