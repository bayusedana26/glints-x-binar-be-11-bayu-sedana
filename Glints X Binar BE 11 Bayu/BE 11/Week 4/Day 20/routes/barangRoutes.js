const express = require("express");
const router = express.Router();
const barangController = require("../controllers/barangContollers");

router.get("/", barangController.getAll);
router.get("/:id", barangController.getOne);
router.post("/", barangController.createBarang);
// router.put("/", barangController.update);
router.delete("/", barangController.deleteBarang);

module.exports = router;
