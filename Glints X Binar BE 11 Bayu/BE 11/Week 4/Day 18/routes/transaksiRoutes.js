const express = require("express"); // Import express
const router = express.Router(); // Make router from express

const transaksiController = require("../controllers/transaksiController"); // import controllers

router.get("/", transaksiController.getAll); // Define routes for get request
router.get("/:id", transaksiController.getOne)
router.post("/", transaksiController.postOne); // Define routes for post request
router.put("/", transaksiController.putAll); // Define routes for put operations
router.delete("/:id", transaksiController.deleteData); // Define routes

module.exports = router;
