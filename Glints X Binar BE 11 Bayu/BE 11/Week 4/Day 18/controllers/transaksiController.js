const connection = require("../models/connection");

const getAll = (req, res) => {
  try {
    // This is query
    let sql =
      "select t.id, p.nama as nama_pelanggan , b.nama as nama_barang, pem.nama as nama_pemasok ,b.harga as harga_barang , t.waktu, t.jumlah, t.total from transaksi t join barang b on t.id_barang = b.id join pelanggan p on p.id = t.id_pelanggan join pemasok pem on b.id_pemasok = pem.id";
    // Run query
    connection.query(sql, (err, result) => {
      if (err) {
        return res.status(500).json({
          message: "Internal server error",
          error: err,
        });
      }
      return res.status(200).json({
        message: "This is get",
        data: result,
      });
    });
  } catch (err) {
    return res.status(500).json({
      message: "Internal server error",
      error: err,
    });
  }
};

const getOne = (req, res) => {
  // This is query
  let sql = `select t.id, p.nama as nama_pelanggan , b.nama as nama_barang, pem.nama as nama_pemasok ,b.harga as harga_barang , t.waktu, t.jumlah, t.total from transaksi t join barang b on t.id_barang = b.id join pelanggan p on p.id = t.id_pelanggan join pemasok pem on b.id_pemasok = pem.id where t.id = ${req.params.id}`;
  // Run query
  connection.query(sql, (err, result) => {
    if (err) {
      return res.status(500).json({
        message: "Internal server error",
        error: err,
      });
    }
    return res.status(200).json({
      message: "Success",
      data: result[0],
    });
  });
};

const postOne = (req, res) => {
  try {
    // find barang to get the price
    let sqlFindBarang = `select * from barang where id = ${req.body.id_barang}`;
    
    // Run query
    connection.query(sqlFindBarang, (err, result) => {
      let price = eval(result[0].harga);
      let total = eval(price * req.body.jumlah);
      // This is query
      let sql = `insert into transaksi (id_barang, id_pelanggan, jumlah, total) values (${req.body.id_barang}, ${req.body.id_pelanggan}, ${req.body.jumlah}, ${total})`;
      // Run query
      connection.query(sql, (err, result) => {
        if (err) {
          return res.status(500).json({
            message: "Internal server error",
            error: err,
          });
        }
        return res.status(200).json({
          message: "This is post",
          data: result,
        });
      });
    });
  } catch (err) {
    return res.status(500).json({
      message: "Internal server error",
      error: err,
    });
  }
};

const putAll = (req, res) => {
  try {
    // This is query
    let sql =
      "select t.id, p.nama as nama_pelanggan , b.nama as nama_barang, pem.nama as nama_pemasok ,b.harga as harga_barang , t.waktu, t.jumlah, t.total from transaksi t join barang b on t.id_barang = b.id join pelanggan p on p.id = t.id_pelanggan join pemasok pem on b.id_pemasok = pem.id";
    // Run query
    connection.query(sql, (err, result) => {
      if (err) {
        return res.status(500).json({
          message: "Internal server error",
          error: err,
        });
      }
      return res.status(200).json({
        message: "This is put",
        data: result,
      });
    });
  } catch (err) {
    return res.status(500).json({
      message: "Internal server error",
      error: err,
    });
  }
};

const deleteData = (req, res) => {
  // Delete Query
  let sql = "DELETE FROM transaksi WHERE id = ?";

  // Run Query
  connection.query(sql, [req.params.id], (err, result) => {
    if (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }

    return res.status(200).json({
      message: "Success",
      data: result,
    });
  });
};

module.exports = { getAll, getOne, postOne, putAll, deleteData };
