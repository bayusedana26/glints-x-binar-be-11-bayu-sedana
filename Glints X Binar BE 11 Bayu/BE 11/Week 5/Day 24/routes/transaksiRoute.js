const express = require("express");

// Import Validator
// Import Controller
const transaksiController = require("../controllers/transaksiController")

// Make router
const router = express.Router();

// Get all function
router.get("/", transaksiController.getAll)

module.exports = router;
