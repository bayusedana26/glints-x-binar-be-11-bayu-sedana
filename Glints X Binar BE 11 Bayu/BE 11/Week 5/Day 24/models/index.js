const mongoose = require("mongoose"); // Import Mongoose

const uri = process.env.MONGO_URI; // Add URI Mongo Atlas

// Connect to MongoDB
mongoose
  .connect(uri, {
    useUnifiedTopology: true,
    useNewUrlParser: true,
    useCreateIndex: true,
    useFindAndModify: false,
  })
  .then(() => {
    console.log("Connected to MongoDB!");
  })
  .catch((err) => {
    console.error(err);
  });

//   Import models
const barang = require("./barang");
const pelanggan = require("./pelanggan");
const pemasok = require("./pemasok");
const transaksi = require("./transaksi");

//   Export models
module.exports = { barang, pelanggan, pemasok, transaksi };
