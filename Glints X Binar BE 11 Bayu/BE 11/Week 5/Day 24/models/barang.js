const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const BarangSchema = new mongoose.Schema(
  {
    nama: {
      type: String,
      required: true,
      unique: true,
    },
    harga: {
      type: Number,
      required: true,
      get: getImage,
    },
    pemasok: {
      type: mongoose.Schema.ObjectId,
      ref: "pemasok",
      required: true,
    },
    image: {
      type: String,
      default: null,
      required: false,
      get: getImage,
    },
  },
  {
    timestamps: {
      createdAt: "createadAt",
      updatedAt: "updatedAt",
    },
  }
);

// Get Images
function getImage(image) {
  return `/images/${image}`;
}

// Enable Soft Delete

BarangSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("barang", BarangSchema, "barang");
