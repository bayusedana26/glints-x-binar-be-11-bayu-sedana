require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
// Express
const express = require("express");
const fileUpload = require("express-fileupload");

// Import router

// Make application
const app = express();

// Body Parser
app.use(express.json());
app.use(express.urlencoded({ extended: true })); // enable req body urlencoded

// Read form data
app.use(fileUpload());

// static folder (for images)
app.use(express.static("public"));

// Make router
const transaksiRoutes = require("./routes/transaksiRoute")

// Make routes
app.use("/transaksi", transaksiRoutes);

// Run server

app.listen(3000, () => console.log("Server running on port 3000"));
