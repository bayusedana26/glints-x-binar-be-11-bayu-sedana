require("dotenv").config({
  path: `.env.${process.env.NODE_ENV}`,
});
const express = require("express"); // Import express
const app = express(); // Make express app
const transaksiRoutes = require("./routes/transaksiRoutes"); // Import routes for transaksi

//Set body parser for HTTP post operation
app.use(express.json()); // support json encoded bodies
app.use(
  express.urlencoded({
    extended: true,
  })
); // support encoded bodies

app.use("/transaksi", transaksiRoutes); // If accessing localhost:3000/transaksi/*, it will go to transaksiRoutes variable

app.listen(3000, () => console.log("Server connected in localhost:3000")); // make port 3000 for this app
