const client = require("../models/index");

class TransaksiController {
  // get All data
  async getAll(req, res) {
    try {
      const penjualan = client.db("penjualan_timC"); // Connect to penjualan database
      const transaksi = penjualan.collection("transaksi"); // Connect to transaksi collection / table

      // find all transaksi data
      let data = await transaksi.find({}).toArray();

      //   if no data found
      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi data not found",
        });
      }
      return res.status(200).json({
        message: "Succes",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}
//             res.json({
//               status: "success",
//               data: result

//     }
//       })
//     }
//   }
// // get one data
// async getOne(req, res) {
//     const penjualan = client.db('penjualan') // Connect to penjualan database
//     const transaksi = penjualan.collection('transaksi') // Connect to transaksi collection / table

//     // Find one data
//     transaksi.findOne({
//       _id: new ObjectId(req.params.id)
//     }).
//       }

module.exports = new TransaksiController();
