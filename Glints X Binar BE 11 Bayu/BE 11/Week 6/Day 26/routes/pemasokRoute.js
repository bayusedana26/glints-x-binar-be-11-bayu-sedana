const express = require("express");

// Import Validator
// Import Controller
const pemasokController = require("../controllers/pemasokController")

// Make router
const router = express.Router();

// Get
router.get("/", pemasokController.getAll)

// Post
router.post("/", pemasokController.create)

// Delete
router.delete("/:id", pemasokController.delete)

module.exports = router;
