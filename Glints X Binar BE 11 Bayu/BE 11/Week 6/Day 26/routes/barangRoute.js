const express = require("express"); // Import express

// Import middlewares
const barangValidator = require("../middlewares/validators/barangValidator");

// Import controller
const barangController = require("../controllers/barangController");

// Make a router
const router = express.Router(); 

// Get all barang data
router.get("/", barangController.getAll);

// Get one barang
router.get("/:id", barangValidator.getOne, barangController.getOne);

// Create barang
router.post("/", barangValidator.create, barangController.create);

// Update barang
router.put("/:id", barangValidator.update, barangController.update);

// Delete barang
router.delete("/:id", /*barangValidator.delete,*/ barangController.delete);

module.exports = router; // Export router
