const express = require("express");

// Import Validator
// Import Controller
const transaksiController = require("../controllers/transaksiController")

// Make router
const router = express.Router();

// Get
router.get("/", transaksiController.getAll)

// Post
router.post("/", transaksiController.create)

// Delete

module.exports = router;
