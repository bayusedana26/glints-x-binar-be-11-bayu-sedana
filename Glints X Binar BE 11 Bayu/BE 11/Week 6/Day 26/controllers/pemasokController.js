const { barang, pelanggan, pemasok, transaksi } = require("../models"); // Import

class PemasokController {
  // Get All data
  async getAll(req, res) {
    try {
      let data = await pemasok.find();
      // If no data
      if (data.length === 0) {
        return res.status(404).json({
          message: "Transaksi not found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async create(req, res) {
    try {
      let creatingData = await pemasok.create({
        nama: req.body.nama,
      });
      console.log(creatingData);
      let data = await pemasok.findOne({
        where: { id: creatingData.id },
        attributes: ["nama", ["createdAt", "waktu terdaftar"]],
      });
      if (data.length === 0) {
        return res.status(404).json({
          message: "pemasok Not Found",
        });
      }
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
  async delete(req, res) {
    try {
      let data = await pemasok.destroy({ where: { _id: req.params.id } });

      if (!data) {
        return res.status(404).json({
          message: "Pemasok Not Found",
        });
      }

      return res.status(200).json({
        message: "Success delete pemasok",
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new PemasokController();
