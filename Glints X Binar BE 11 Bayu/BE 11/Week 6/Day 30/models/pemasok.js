const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const PemasokSchema = new mongoose.Schema(
  {
    nama: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
      default: null,
      required: false,
      get: getPhoto,
    },
  },
  {
    timestamps: {
      createdAt: "createadAt",
      updatedAt: "updatedAt",
    },
    toJSOn: { getters: true },
  }
);

// Get Images
function getPhoto(photo) {
  return `/images/${photo}`;
}

// Enable Soft Delete
PemasokSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("pemasok", PemasokSchema, "pemasok");
