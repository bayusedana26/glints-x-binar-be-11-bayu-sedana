const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import middlewares
const validatorPemasok = require("../middlewares/validators/pemasokValidator");

// Import controller
const controllerPemasok = require("../controllers/pemasokController");

// Import auth
const auth = require("../middlewares/validators/auth/index")

router.get("/", auth.adminOrUser, controllerPemasok.getAll);
router.get("/:id", auth.adminOrUser, controllerPemasok.getOne);
router.post("/", auth.admin, validatorPemasok.create, controllerPemasok.create);
router.put("/:id", auth.admin, validatorPemasok.update, controllerPemasok.update);
router.delete("/:id", auth.admin, controllerPemasok.delete);

module.exports = router; // Export router
