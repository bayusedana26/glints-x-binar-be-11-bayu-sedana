const express = require("express"); // Import express

// Import middlewares
const barangValidator = require("../middlewares/validators/barangValidator");

// Import controller
const barangController = require("../controllers/barangController");

// Import auth
const auth = require("../middlewares/validators/auth/index")

// Make a router
const router = express.Router(); 

// Get all barang data
router.get("/", auth.adminOrUser, barangController.getAll);

// Get one barang
router.get("/:id", auth.adminOrUser, barangValidator.getOne, barangController.getOne);

// Create barang
router.post("/", auth.admin, barangValidator.create, barangController.create);

// Update barang
router.put("/:id", auth.admin, barangValidator.update, barangController.update);

// Delete barang
router.delete("/:id", auth.admin, /*barangValidator.delete,*/ barangController.delete);

module.exports = router; // Export router
