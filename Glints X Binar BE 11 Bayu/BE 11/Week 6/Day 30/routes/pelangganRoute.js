const express = require("express");

// import validator
const pelangganValidator = require("../middlewares/validators/pelangganValidator")

// import controllers
const pelangganController = require("../controllers/pelangganController");

// import auth
const auth = require("../middlewares/validators/auth/index")

// make routers
const router = express.Router();

// get all pelanggan
router.get("/", auth.adminOrUser, pelangganController.getAll);
router.get("/:id", auth.adminOrUser, pelangganValidator.getOne, pelangganController.getOne);
router.post("/", auth.admin, pelangganController.create);
router.put("/:id", auth.admin, pelangganValidator.update, pelangganController.update);
router.delete("/:id", auth.admin, pelangganController.delete);

module.exports = router;

