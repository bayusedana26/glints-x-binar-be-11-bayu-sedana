const mongoose = require("mongoose");
const validator = require("validator");
const { barang, pelanggan, pemasok, transaksi } = require("../../models");

exports.getOne = (req, res, next) => {
  // Check parameter is valid or not
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.status(400).json({
      message: "Parameter is not valid and must be 24 character & hexadecimal",
    });
  }

  next();
};

exports.update = async (req, res, next) => {
  // Check parameter id is valid or not
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    return res.status(400).json({
      message:
        "Pelanggan ID is not valid and must be 24 character & hexadecimal",
    });
  }
  next();
};
