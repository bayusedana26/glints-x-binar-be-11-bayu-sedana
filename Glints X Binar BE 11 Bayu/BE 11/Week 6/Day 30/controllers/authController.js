const jwt = require("jsonwebtoken");

class AuthController {
  // If user sign up/login
  async getToken(req, res) {
    try {
      // Make body variable
      const body = {
        id: req.user._id,
      };
      // Create jwt token
      const token = jwt.sign(
        {
          user: body,
        },
        process.env.JWT_TOKEN
      );
      // If success
      return res.status(200).json({
        message: "Success",
        token,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }
}

module.exports = new AuthController();
