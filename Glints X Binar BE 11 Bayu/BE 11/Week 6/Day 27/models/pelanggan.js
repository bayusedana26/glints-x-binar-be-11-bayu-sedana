const mongoose = require("mongoose");
const mongooseDelete = require("mongoose-delete");

const PelangganSchema = new mongoose.Schema(
  {
    nama: {
      type: String,
      required: true,
    },
    photo: {
      type: String,
      default: null,
      required: false,
      get: getPhoto,
    },
  },
  {
    timestamps: {
      createdAt: "createadAt",
      updatedAt: "updatedAt",
    },
    toJSOn: { getters: true },
  }
);

// Get Images
function getPhoto(photo) {
  return `/images/${photo}`;
}

// Enable Soft Delete
PelangganSchema.plugin(mongooseDelete, { overrideMethods: "all" });

module.exports = mongoose.model("pelanggan", PelangganSchema, "pelanggan");
