const passport = require("passport"); // Import passport
const LocalStrategy = require("passport-local").Strategy; // Import local strategy
const bcrypt = require("bcrypt"); // Import bcrypt
const JWTstrategy = require("passport-jwt").Strategy; // Import jwt strategy
const ExtractJwt = require("passport-jwt").ExtractJwt; // Import extract jwt strategy
const { user } = require("../../../models"); // Import all models

// If user sign up
passport.use(
  "signup",
  new LocalStrategy(
    {
      usernameField: "email", // Req.body.email
      passowrdField: "password", // Req.body.password
      passReqToCallback: true, // Enable to read req.body / params / query
    },
    async (req, email, password, done) => {
      try {
        // Create User
        let userSignup = await user.create(req.body);

        // If success
        return done(null, userSignup, {
          message: "User can be created",
        });
      } catch (error) {
        // If error
        return done(null, false, {
          message: " User cannot be created",
        });
      }
    }
  )
);

passport.use(
  "signin",
  new LocalStrategy(
    {
      usernameField: "email", // Req.body.email
      passowrdField: "password", // Req.body.password
      passReqToCallback: true, // Enable to read req.body / params / query
    },
    async (req, email, password, done) => {
      try {
        // Create User
        let userSignin = await user.findOne({ email})

        // If success
        return done(null, userSignin, {
          message: "User can be created",
        });
// check password
let validate = await bcrypt.compare(compare, userSignin.password)

// if wrong password
      } catch (error) {
        // If error
        return done(null, false, {
          message: " User cannot be created",
        });
      }
    }
  )
);
