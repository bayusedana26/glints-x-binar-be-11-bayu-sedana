const mongoose = require("mongoose");
const crypto = require("crypto");
const path = require("path");
const validator = require("validator");
const { barang, pelanggan, pemasok, transaksi } = require("../../models");

exports.create = async (req, res, next) => {
  let errors = [];
  if (!validator.isAlpha(req.body.nama)) {
    errors.push("nama must be in alphabet without space");
  }

  // If errors length > 0, it will make errors message

  
  console.log(path);
  // req.body.nama = req.files.nama
  if (errors.length > 0) {
    // Because bad request
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  next();
};

exports.update = async (req, res, next) => {
  let errors = [];
  if (!mongoose.Types.ObjectId.isValid(req.params.id)) {
    errors.push(
      "id_pemasok is not valid and must be 24 character & hexadecimal"
    );
  }
  if (!validator.isAlpha(req.body.nama)) {
    errors.push("nama must be in alphabet without space");
  }

  // If errors length > 0, it will make errors message

  if (req.files) {
    // If image was uploaded

    const file = req.files.image;

    // Make sure image is photo
    if (!file.mimetype.startsWith("image")) {
      return res.status(400).json({ message: "File must be an image" });
    }

    // Check file size (max 1MB)
    if (file.size > 1000000) {
      return res.status(400).json({ message: "Image must be less than 1MB" });
    }

    // Create custom filename
    let fileName = await crypto.randomBytes(16).toString("hex");

    // Rename the file
    file.name = `${fileName}${path.parse(file.name).ext}`;
    console.log(req.files.image);

    // assign req.body.image with file.name
    req.body.image = file.name;

    // Upload image to /public/images
    file.mv(`./public/images/${file.name}`, async (err) => {
      if (err) {
        console.error(err);

        return res.status(500).json({
          message: "image failed, server error",
          error: err,
        });
      }
    });
  }
  // req.body.nama = req.files.nama
  if (errors.length > 0) {
    // Because bad request
    return res.status(400).json({
      message: errors.join(", "),
    });
  }

  next();
};
