const express = require("express"); // Import express
const router = express.Router(); // Make a router

// Import middlewares
const validatorPemasok = require("../middlewares/validators/pemasokValidator");

// Import controller
const controllerPemasok = require("../controllers/pemasokController");

router.get("/", controllerPemasok.getAll);
router.get("/:id", controllerPemasok.getOne);
router.post("/", validatorPemasok.create, controllerPemasok.create);
router.put("/:id", validatorPemasok.update, controllerPemasok.update);
router.delete("/:id", controllerPemasok.delete);

module.exports = router; // Export router
