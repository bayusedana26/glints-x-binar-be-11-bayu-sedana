const express = require("express"); // Import Express
const passport = require("passport"); // Import passport

// Import Validator

// Import Controller
const authController = require("../controllers/authController");

// Import auth from middlewares
require("../middlewares/validators/auth/index");

// Make router
const router = express.Router();

router.post(
  "/signup",
  async (req, res, next) => {
    passport.authenticate("signup", { session: false }, (err, user, info) => {
      if (err) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: err,
        });
      }
      // User is false
      if (!user) {
        return res.status(401).json({
          message: info.message,
        });
      }
      req.user = user;
      next();
    })(req, res, next);
  },
  authController.getToken
);

router.post(
  "/signin",
  async (req, res, next) => {
    passport.authenticate("signin", { session: false }, (err, user, info) => {
      if (err) {
        return res.status(500).json({
          message: "Internal Server Error",
          error: err,
        });
      }
      // User is false
      if (!user) {
        return res.status(401).json({
          message: info.message,
        });
      }
      req.user = user;
      next();
    })(req, res, next);
  },
  authController.getToken
);

module.exports = router;
