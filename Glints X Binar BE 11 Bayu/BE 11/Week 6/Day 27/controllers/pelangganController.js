const { barang, pelanggan, pemasok, transaksi } = require("../models");

class PelangganController {
  // Get All
  async getAll(req, res) {
    try {
      // Find all data
      let data = await pelanggan.find();

      // If no data
      if (data.length === 0) {
        return res.status(404).json({
          message: "Pelanggan Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Get One
  async getOne(req, res) {
    try {
      // Find one data from pelanggan
      let data = await pelanggan.findOne({ _id: req.params.id });

      // If pelanggan not found
      if (!data) {
        return res.status(404).json({
          message: "Pelanggan Not Found",
        });
      }

      // If success
      return res.status(200).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // Create transaksi
  async create(req, res) {
    try {
      // Create data
      let data = await pelanggan.create(req.body);

      // If success
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (e) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: e,
      });
    }
  }

  // update pelanggan
  async update(req, res) {
    try {
      let data = await pelanggan.findOneAndUpdate(
        {
          _id: req.params.id,
        },
        req.body,
        {
          new: true,
        }
      );

      // if success
      return res.status(201).json({
        message: "Success",
        data,
      });
    } catch (err) {
      return (
        res,
        status(500).json({
          message: "Internal Server Error",
          error: err,
        })
      );
    }
  }

  async delete(req, res) {
    try {
      await pelanggan.delete({ _id: req.params.id });

      return res.status(200).json({
        message: "Success",
      });
    } catch (err) {
      return res.status(500).json({
        message: "Internal Server Error",
        error: err,
      });
    }
  }
}

module.exports = new PelangganController();
