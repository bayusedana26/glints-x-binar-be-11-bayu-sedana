const index = require("../index"); // Import index to run rl on this file

// Function to calculate cylinder volume
function cylinderVolume(radius, height) {
  return Math.PI * radius ** 2 * height;
}

/* Way 1 */
// Function for inputing cylinder
function inputRadius() {
  index.rl.question(`Radius: `, (radius) => {
    if (!isNaN(radius)) {
      inputHeight(radius);
    } else {
      console.log(`Radius must be a number\n`);
      inputRadius();
    }
  });
}

// Function for inputing height of cylinder
function inputHeight(radius) {
  index.rl.question(`Height: `, (height) => {
    if (!isNaN(height)) {
      console.log(`\nVolume cylinder: ${cylinderVolume(radius, height)}`);
      index.rl.close();
    } else {
      console.log(`Height must be a number\n`); //\n untuk bikin line baru
      inputHeight(radius);
    }
  });
}

module.exports = { inputRadius }; // Export the input, so the another file can run this code
