function sum(array) {
  const result = array.reduce((accumulatorRow, currentValueRow) => {
    return (
      accumulatorRow +
      currentValueRow.reduce(
        (accumulatorCol, currentValueCol) => accumulatorCol + currentValueCol
      )
    );
  }, 0);
  return result;
}

const numbers = [
  [1, 2, 3],
  [4, 5, 6],
  [7, 8, 9],
];

console.log(sum(numbers)); // 45
