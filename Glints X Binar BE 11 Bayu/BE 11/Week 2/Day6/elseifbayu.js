function check(value) {
  if (value >= 90) {
    return "Sangat Bagus";
  } else if (value >= 80) {
    return "Bagus";
  } else if (value >= 70) {
    return "Sangat Cukup";
  } else {
    return "Cukup";
  }
}
console.log(check(95));
console.log(check(85));
console.log(check(75));
console.log(check(50));
