let result = 0;

function simpleRecursive(number) {
  let newNumber = number - 1;
  result += number;
  if (newNumber === 0) {
    return;
  } else {
    simpleRecursive(newNumber);
  }
}

simpleRecursive(5);
console.log(result);
