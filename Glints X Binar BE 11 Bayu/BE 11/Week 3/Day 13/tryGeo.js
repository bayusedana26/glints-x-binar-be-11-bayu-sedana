const { Square, Rectangle, Triangle, Beam, Cube, Tube, Cone } = require("./geometry");

let squareOne = new Square(12);
squareOne.calculateArea();

let triangleOne = new Triangle(10,12)
triangleOne.calculateArea();

let rectangleOne = new Rectangle(11, 12);
rectangleOne.calculateArea();

let cubeOne = new Cube(10);
cubeOne.calculateArea();

let tubeOne = new Tube(12, 10);
tubeOne.calculateArea();

let beamOne = new Beam(10, 11, 12);
beamOne.calculateArea();

let coneOne = new Cone(17, 20);
coneOne.calculateArea();
