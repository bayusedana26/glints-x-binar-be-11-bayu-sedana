const Geometry = require('./geometry')

class ThreeDimention extends Geometry {
    constructor(name) {
        super(name, "3D")
        
        if(this.constructor === ThreeDimention){
            throw new Error("Can not declare object")
        }
    }
    introduce() {
        super.introduce()
        console.log(`this is ${this.name}!`)
        console.log(`this is ${this.type}!`)
    }
    calculateArea(){
        console.log(`${this.name} Area!`)
    }
    calculateCircumference(){
        console.log(`${this.name} Circumference!`)
    }
}

// let tryThreeD = new ThreeDimention("Three Dimention")
// tryThreeD.introduce()
module.exports = ThreeDimention