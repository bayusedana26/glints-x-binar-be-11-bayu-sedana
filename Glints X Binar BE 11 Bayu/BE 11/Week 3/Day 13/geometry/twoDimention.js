const Geometry = require("./geometry");

class TwoDimention extends Geometry {
  constructor(name) {
    super(name, "2D");

    if (this.constructor === TwoDimention) {
      throw new Error("Can not declare object");
    }
  }
  introduce() {
    super.introduce();
    console.log(`this is ${this.name}!`);
    console.log(`this is ${this.type}!`);
  }
  calculateArea() {
    console.log(`${this.name} Area!`);
  }
  calculateCircumference() {
    console.log(`${this.name} Circumference!`);
  }
}

// let tryTwoD = new TwoDimention("Two Dimention")
// tryTwoD.introduce()
module.exports = TwoDimention;
