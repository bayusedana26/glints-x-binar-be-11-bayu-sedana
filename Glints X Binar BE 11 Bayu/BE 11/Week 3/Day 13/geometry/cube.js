const ThreeDimention = require("./threeDimention");

class Cube extends ThreeDimention {
  constructor(side) {
    super("Cube");
    this.side = side;
  }
  introduce(who) {
    super.introduce();
    console.log(`${who}, this is ${this.name}`);
  }
  calculateArea() {
    super.calculateArea();
    let area = this.side ** 3; //
    console.log(`${this.name} area is ${area} cm2 \n`);
  }
}

module.exports = Cube;
