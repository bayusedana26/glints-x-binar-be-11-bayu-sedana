const Square = require("./square");
const Rectangle = require("./rectangle");
const Triangle = require("./triangle");
const Beam = require("./beam");
const Cone = require("./cone");
const Cube = require("./cube");
const Tube = require("./tube");

module.exports = { Square, Rectangle, Triangle, Beam, Cube, Tube, Cone };
