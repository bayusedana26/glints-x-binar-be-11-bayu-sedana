const ThreeDimention = require("./threeDimention");

class Beam extends ThreeDimention {
  constructor(length, breadth, depth) {
    super("Beam");
    this.length = length;
    this.depth = depth;
    this.breadth = breadth;
  }
  introduce(who) {
    super.introduce();
    console.log(`${who}, this is ${this.name}`);
  }
  calculateArea() {
    super.calculateArea();
    let area = this.breadth * this.depth * this.length;
    console.log(`${this.name} area is ${area} cm2 \n`);
  }
}

module.exports = Beam;
