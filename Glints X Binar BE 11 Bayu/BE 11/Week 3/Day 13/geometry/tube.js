const ThreeDimention = require("./threeDimention");

class Tube extends ThreeDimention {
  constructor(radius, height) {
    super("Tube");
    this.radius = radius;
    this.height = height;
  }
  introduce(who) {
    super.introduce();
    console.log(`${who}, this is ${this.name}`);
  }
  calculateArea() {
    super.calculateArea();
    let area = Math.PI * this.radius ** 2 * this.height;
    console.log(`${this.name} area is ${area} cm2 \n`);
  }
}

module.exports = Tube;
