const TwoDimention = require("./twoDimention");

class Square extends TwoDimention {
  constructor(length) {
    super("Square");
    this.length = length;
  }
  introduce(who) {
    super.introduce();
    console.log(`${who}, this is ${this.name}`);
  }
  calculateArea() {
    super.calculateArea();
    let area = this.length ** 2;
    console.log(`${this.name} area is ${area} cm2 \n`);
  }
  calculateCircumference() {
    super.calculateCircumference();
    let circumference = this.length * 4;
    console.log(`${this.name} area is ${circumference} cm \n`);
  }
}

// let squareOne = new Square(11);
// squareOne.introduce("Bayu");

module.exports = Square;
