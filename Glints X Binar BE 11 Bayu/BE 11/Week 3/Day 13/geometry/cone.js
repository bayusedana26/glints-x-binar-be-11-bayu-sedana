const ThreeDimention = require("./threeDimention");

class Cone extends ThreeDimention {
  constructor(radius, height) {
    super("Cone");
    this.radius = radius;
    this.height = height;
  }
  introduce(who) {
    super.introduce();
    console.log(`${who}, this is ${this.name}`);
  }
  calculateArea() {
    super.calculateArea();
    let area = (1 / 3) * Math.PI * this.radius ** 2 * this.height;
    console.log(`${this.name} area is ${area} cm2 \n`);
  }
}

module.exports = Cone;
