class Geometry {
    constructor(name, type) {
        if(this.constructor == Geometry) {
            throw new Error("Can not declare object")
        }
        this.name = name;
        this.type = type;
    }
    introduce() {
        console.log("This is geometry!")
        this.#accessIntroduce();
    }
    #accessIntroduce() {
        console.log("This is private!")
    }
}
module.exports = Geometry