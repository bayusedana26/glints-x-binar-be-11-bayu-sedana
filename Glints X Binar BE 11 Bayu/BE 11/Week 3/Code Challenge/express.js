const express = require(`express`);
const app = express();
const port = 3000;

app.get("/:name", (req, res) => {
  console.log("This is get");
  res.send(`${req.params.name} Sedana`);
});
app.post("/:name", (req, res) => {
  console.log("This is post");
  res.send(`${req.params.name} Sedana`);
});
app.put("/:name", (req, res) => {
  console.log("This is put");
  res.send(`${req.params.name} Sedana`);
});
app.delete("/:name", (req, res) => {
  console.log("This is delete");
  res.send(`${req.params.name} Sedana`);
});

app.listen(port, () => {
  console.log("Localhost running on", port);
});
