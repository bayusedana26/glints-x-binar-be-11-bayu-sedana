// Import fs
const fs = require("fs");

// Make promise object

const readFile = (file, options) =>
  new Promise((success, failed) => {
    fs.readFile(file, options, (err, content) => {
      if (err) failed(err);
      return success(content);
    });
  });

const readAllFile = async () => {
  try {
    let data = await Promise.all([
      readFile("./content1.txt", "utf-8"),
      readFile("./content2.txt", "utf-8"),
      readFile("./content3.txt", "utf-8"),
      readFile("./content4.txt", "utf-8"),
      readFile("./content5.txt", "utf-8"),
      readFile("./content6.txt", "utf-8"),
      readFile("./content7.txt", "utf-8"),
      readFile("./content8.txt", "utf-8"),
      readFile("./content9.txt", "utf-8"),
      readFile("./content10.txt", "utf-8"),
    ]);
    // let content1 = await readFile("./content1.txt", "utf-8");
    // let content2 = await readFile("./content2.txt", "utf-8");
    // console.log(content1 + " " + content2);
    let content =
      data[0] +
      data[1] +
      data[2] +
      data[3] +
      data[4] +
      data[5] +
      data[6] +
      data[7] +
      data[8] +
      data[9];
    console.log(content);
  } catch (e) {
    console.log(e);
  }
};

readAllFile();
