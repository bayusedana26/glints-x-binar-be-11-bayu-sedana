// Normal function
function calculate(x, y) {
  return x * y;
}

console.log(calculate(15, 16));

// Arrow function

const calc = (x, y) => {
  return x * y;
};
console.log(calculate(15, 16));

// Currying function

const calcu = (x) => (y) => (z) => {
  return x * y + z;
};
console.log(calcu(15)(16)(0));

// Arrow function return

const calcuta = (x, y) => x * y;

console.log(calcuta(15, 16));
