// Initialize an instance because it is a class
const EventEmitter = require("events");
const readline = require("readline");
const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});
const my = new EventEmitter();

// Registering a listener
my.on("Login Failed", function (email) {
  // TODO: Saving the login trial count in the database
  console.log(email, "is failed to login!");
  rl.close();
});
my.on("Login Success", function (email) {
  console.log(email, "your'e logged!");
  require("../../Week 2/Day10/assignment2");
  rl.close();
});
const user = {
  login(email, password) {
    const passwordStoredInDatabase = "123456";
    const emailStoredInDatabase = "Bayu@mail.com"

    if (password != passwordStoredInDatabase && email != emailStoredInDatabase) {
      my.emit("Login Failed", email); // Pass the email to the listener
    } else {
      my.emit("Login Success", email);
    }
  },
};

rl.question("Email: ", function (email) {
  rl.question("Password: ", function (password) {
    user.login(email, password); // Run login function
  });
});
