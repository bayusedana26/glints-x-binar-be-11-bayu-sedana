const EventEmitter = require ("events") // Imports evens module
const my = new EventEmitter () // Declare new event

// Event Listener
my.on("Mas Irvan", ()=> {
    console.log("Halo mas Irvan")
})

my.emit("Mas Irvan")
my.emit("Mas Irvan")
my.emit("Mas Irvan")