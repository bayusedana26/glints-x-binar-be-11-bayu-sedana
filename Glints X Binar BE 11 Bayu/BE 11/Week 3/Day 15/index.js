const express = require("express");
const app = express(); // Make Express App
const routes = require("./Routes/routes");

app.use("/", routes);

app.listen(3000, () => console.log("Server running on 3000"));
