const express = require("express"); // Import Express
const router = express.Router(); // Make a router
const Controller = require("../Controller/controller");

router.get("/:name", Controller.get);
router.get("/", Controller.post);
router.get("/", Controller.put);
router.get("/", Controller.delete);

module.exports = router;
