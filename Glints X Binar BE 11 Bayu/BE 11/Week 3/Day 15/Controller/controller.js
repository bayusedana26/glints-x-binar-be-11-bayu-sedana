// Make a classs

class Controller {
  get(req, res) {
    console.log("This is my first get!"); // console log bisa berkali-kali
    // res.send(`Hello, get!, ${req.query.name}, ${req.query.address}`);
      res.send(`${req.params.name} Sedana`);
  }
  post(req, res) {
    console.log("This is my first post!");
    res.send("Hello, post!");
  }
  put(req, res) {
    console.log("This is my first put!");
    res.send("Hello, put!");
  }
  delete(req, res) {
    console.log("This is my first delete!");
    res.send("Hello, delete!");
  }
}

module.exports = new Controller();
