const TwoDimention = require("./twoDimention");

class Square extends TwoDimention {
  constructor(length) {
    super("Square");
    this.length = length;
  }
  introduce() {
    super.introduce();
    console.log(`This is ${this.name}`);
  }
  caclulateArea(message) {
    super.caclulateArea();
    console.log(`${message} ${this.length ** 2}`);
  }
}
let squareOne = new Square(15);
squareOne.introduce();
squareOne.caclulateArea("Square Area");

