class Person {
  static isAlive = true; // static property

  // constructor and instance property
  constructor(fullName, height, weight, sex) {
    this.fullName = fullName;
    this.height = height;
    this.weight = weight;
    this.sex = sex;
  }
  // instance method
  student() {
    console.log(`${this.fullName} is Junior School Student`);
  }
  // instance method
  undergraduate() {
    console.log(`${this.fullName} is Undergraduate student`);
  }
  typeclass() {
    this.student();
    this.undergraduate();
  }
  static isEating() {
    console.log(`Someone is eating!`);
  }
}
Person.prototype.watch = function () {
    console.log(`${this.fullName} is watching Netflix`)
}
Person.sleep = function() { 
    console.log(`Someone is sleeping`)
}

let Person1 = new Person("Bayu Sedana", 172, 75, "Male");
let Person2 = new Person("Fitriana", 153, 40, "Female");

console.log(Person1);
console.log(Person.isAlive);
Person1.watch();
Person.isEating();
Person.sleep();

console.log(Person2);
console.log(Person.isAlive);
Person.isEating();
Person2.watch();
Person.sleep();

Person1.typeclass();
Person2.typeclass();

Person1.undergraduate();
Person2.student();
