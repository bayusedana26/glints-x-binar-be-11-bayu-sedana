class Geometry {
  constructor(name, type) {
    this.name = name;
    this.type = type;
  }
  introduce() {
    console.log(`This is geometry`);
  }
}
module.exports = Geometry;
